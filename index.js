const tabsContainer = document.querySelector('.centered-content');
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItems = document.querySelectorAll(".tabs-item");

tabsContainer.addEventListener('click', function(event) {
    if (event.target.classList.contains('tabs-title')) {
        const currentTitle = event.target;
        const tabId = currentTitle.getAttribute('data-tab');
        const contentTab = document.querySelector(tabId);

        tabsTitle.forEach(function (item){
            item.classList.remove("active");
        });

        tabsItems.forEach(function (item){
            item.classList.remove("active");
        });

        currentTitle.classList.add("active");
        contentTab.classList.add("active");
    }
});
